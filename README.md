# Bigger Picture - task for Front-end Developer candidate #

The purpose of this task is to develop front-end code of a single page, making it fully responsive. We'd like to see how you use your coding skills to create an optimised, speedy website which looks awesome on each viewport (we have purposely not supplied a design for mobile, tablet etc. We want to see what you'd do!). We also want to see how you use your dev skills in a creative way - we're huge fans of micro-animation so wow us with on-hover effects, funky links and other animation touches - go crazy!

Preview: http://frontend-task.devstage.co.uk/ - this is how we (developers) usually get the project to start coding. The current HTML/CSS/JS you can see over there is created by our design team and not meant to be 'production ready.' The code is invalid, unsemantic, unresponsive, unoptimised etc. **Your job is to re-code what you see, with additional tasks described below**

### To do ###
1. Your code needs to be written with Google Web Starter Kit (https://developers.google.com/web/tools/starter-kit/). We use this boilerplate as a starting point of any front-end project we create. You should create your own repository somewhere (Bitbucket, Github) and place your code over there so you can send us a link to the repo with ready website.
2. Re-code the current page http://frontend-task.devstage.co.uk using HTML, CSS (Sass) and JS. Your work should follow all best development practices. You should include the same animation in the hero banner (maybe written in better way - the freedom is yours!).
3. The page should be looking awesome on mobile, tablet, small/medium/large desktop etc. - fully responsive. Use your creativity to create an awesome responsive look.
4. All the CTA buttons, links in the top navigation etc. should have on-hover effects. Surprise us with :-) We love to get inspired from https://tympanus.net. 
5. Try to use BEM methodology in writing SCSS (http://getbem.com). We use it a lot and we expect this knowledge from every candidate.
6. Clicking the triangle button on the bottom of hero banner should scroll user down to the first section after the banner in a smooth way.
7. In the top navigation you can see a language icon - it should reveal some dropdown with a few more languages - say english, polish and spanish. Create something gorgeous!
8. In the top navigation there is a search icon too - clicking the icon should make something funky happen - take a look at some inspiration in terms of UI effect at https://tympanus.net/Development/SearchUIEffects/ and implement it. Or, create something even better!
9. Below the "SIGMA-HSE" section, you have to create one more section which is not visible on the preview - you can only see the section on the video we prepared for you at http://frontend-task.devstage.co.uk/slider-preview.mp4. You should create this section according to the screencast, preserving the behaviour and the effect.

### Additional info ###
1. Fonts - please take a look at the source code of http://frontend-task.devstage.co.uk and you will find Google Web Fonts imported. Please make sure all the font weights are needed and if not, import only the weights that are really needed.
2. Additional points will be granted for ES6.
3. All the images/assets you need are in the repository.
3. We expect a link to your github/bitbucket repository with a working page, but you can also upload your work somewhere else, so we can quickly take a look.
4. Delivery time: We get that you'll be busy but expect something back in a week. Impress us and you'll have a shiny new computer and challenging projects to work on very soon!
5. If you have any questions, please do not hesitate to contact our Head of Technology, Bartek Mis - bartek@biggerpicture.agency.

Good luck, and we can't wait to see!